<?php

use App\Http\Controllers\posts\PostController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\TestModelController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('test');
});

Route::get('/test/{name}',[TestController::class,'show']);

// Route::group([],function($route){
//  $route->get('/test/{$name}',[TestController::class,'show']);
// });


Route::get('/testmodel/{test}',[TestModelController::class,'index']);

Route::resource('/post/create',PostController::class)->only(['show','create']);


